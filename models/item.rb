class Item
  include DataMapper::Resource

  property :id,    Serial
  property :name,  String,  required: true
  property :count, Integer, required: true, min: 1
  property :price, Integer, required: true, min: 1

  belongs_to :expense

  def cost
    count * price
  end
end
