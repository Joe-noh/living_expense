require_relative './item'
require_relative './expense'

module Model
  def setup(env)
    db_config = Sinatra::Application.settings.config[:mysql]
    uri = "mysql://#{db_config[:username]}:#{db_config[:password]}@#{db_config[:host]}/expense"

    case env
    when :production, 'production'
      DataMapper.setup :default, uri
    when :development, 'development'
      DataMapper.setup :default, uri+"_dev"
    else
      DataMapper.setup :default, 'sqlite::memory:'
    end

    DataMapper.finalize
    DataMapper.auto_upgrade!
  end

  module_function :setup
end
