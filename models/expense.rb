# -*- coding: utf-8 -*-

class Expense
  include DataMapper::Resource

  property :id,          Serial
  property :purpose,     String, required: true
  property :responsible, String, required: true
  property :payday,      Date,   required: true
  property :income,      Boolean

  has n, :items

  validates_presence_of :items

  def total_cost
    items.inject(0){|sum, item| sum + item.cost }
  end

  def readable_payday
    payday.strftime "%Y年 %-m月 %-d日"
  end

  def itemize
    items > 1 ? "itemize" : ""
  end
end
