$(function () {
    $('.itemize').click(function() {
	$("#item-modal h2").remove();
	$("#item-modal thead").remove();
	$("#item-modal tbody").remove();

	var expenseId = $(this).data('id');
	var title = $("<h2></h2>");
	var thead = $("<thead></thead>");
	var tr    = $("<tr></tr>");
	var tbody = $("<tbody></tbody>");
	var modalTable = $('#modal-table');

	jQuery.getJSON('/expense/json/'+expenseId, function(expense) {
	    title.text(expense.purpose);

	    tr.append("<th>Item</th>");
	    tr.append("<th>Count</th>");
	    tr.append("<th>Price</th>");
	    thead.append(tr);
	    modalTable.prepend(thead);

	    $.each(expense.items, function() {
		var tr = $("<tr></tr>");
		tr.append("<td>"+this.name+"</td>");
		tr.append("<td>"+this.count+"</td>");
		tr.append("<td>"+this.price+"</td>");
		tbody.append(tr);
	    });
	});
	modalTable.append(tbody);

	var content = $("#item-modal > div.content");
	content.prepend(title);

	$("#item-modal form").attr("action", "/expense/"+expenseId);

        $("#item-modal").addClass('active');
    });

    $('#one-more-button').click(function() {
	var itemForm = $('div.input-each-item:last');
	var clone = itemForm.clone(false);

	$("input", clone).val("");
	clone.insertAfter(itemForm);
    });

});
