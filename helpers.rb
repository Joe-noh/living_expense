
module HelperMethods
  def user_list
    ldap_config = settings.config[:ldap]
    params = ldap_config.except(:filter, :attribute)
    params[:auth][:method] = params[:auth][:method].to_sym  # dirty
    ldap = Net::LDAP.new params

    filter = Net::LDAP::Filter.eq *ldap_config[:filter].flatten.map(&:to_s)
    attribute = ldap_config[:attribute]

    ldap.open { |agent|
      agent.search(filter: filter, attributes: [attribute], return_result: true)
    }.map{ |entry|
      entry[attribute]
    }.flatten
  end
end

helpers { include HelperMethods }
