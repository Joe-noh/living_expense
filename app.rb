require 'bundler'

ENV['RACK_ENV'] ||= 'development'
Bundler.require(:default, ENV['RACK_ENV'].to_sym)

require 'json'
require_relative './extends/integer'
require_relative './extends/hash'

set :config, YAML.load_file("./config.yml").symbolize_keys

require_relative './helpers'
require_relative './models/model'

Model.setup(settings.environment)

get '/' do
  @expenses = Expense.all(order: [:payday.asc])
  rest = @expenses.inject(0){|acc, exp| acc.send(exp.income ? :+ : :-, exp.total_cost) }

  @klass = ""
  @rest = "&yen;#{rest.abs.comma_inserted}"
  if rest > 0
    @klass = "plus"
  else
    @klass = "minus"
    @rest = "-#{@rest}"
  end

  @users = user_list

  erb :index
end

post '/' do
  expense_params = params.slice_as_symbol('purpose', 'responsible', 'payday', 'income')
  expense = Expense.create(expense_params)

  names  = params['item-names']
  prices = params['item-prices'].map(&:to_i)
  counts = params['item-counts'].map(&:to_i)

  names.zip(prices, counts).each do |name, price, count|
    item = Item.create(name: name, price: price, count: count)
    expense.items << item
  end

  expense.save

  redirect '/expense/', 303
end

get '/json/:expense_id' do
  content_type :json
  expense = Expense.get(params[:expense_id].to_i)
  {purpose: expense.purpose, items: expense.items}.to_json
end

delete '/:id' do
  expense = Expense.get(params[:id].to_i)
  expense.items.each(&:destroy!)
  expense.destroy!

  redirect '/expense/', 303
end

