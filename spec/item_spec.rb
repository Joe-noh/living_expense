# -*- coding: utf-8 -*-

require_relative './spec_helper'

before do
  10.times{ Expense.gen! }
end

describe Item do

  describe 'DBスキーマ' do
    it '空でない品目名を持つ' do
      normals   = ["えんぴつ", "宴会", "party"]
      abnormals = ["", "  ", nil]

      validation_test(Item, :name, normals, abnormals)
    end

    it '1以上の数量を持つ' do
      normals   = [1, 3, "30", 100]
      abnormals = [0, -2, nil]

      validation_test(Item, :count, normals, abnormals)
    end

    it '1以上の単価を持つ' do
      normals   = [1, 3, "30", 100]
      abnormals = [0, -2, nil]

      validation_test(Item, :price, normals, abnormals)
    end
  end

  describe 'メソッド' do
    it '合計金額を返せる' do
      item = Item.first
      cost = item.price * item.count
      expect(cost).to eql item.cost
    end
  end
end
