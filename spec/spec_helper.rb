ENV['RACK_ENV'] = 'test'

require_relative '../app'

set :environment,  :test
set :raise_errors, true
set :logging,      false

def app
  LivingExpense
end

RSpec.configure do |conf|
  conf.include Rack::Test::Methods
end

## Helpers ##
def validation_test(model, attr, normals, abnormals)
  obj = model.first
  {normals => be_true, abnormals => be_false}.each do |cases, be_expected|
    cases.each do |val|
      method = attr.to_s + '='
      obj.send(method, val)
      expect(obj.valid?).to be_expected
    end
  end
end


## Fixtures ##

Expense.fix {{
    purpose: /w{1,3}/.gen,
    responsible: /w{1}/.gen,
    payday: Date.today,
    items: (1..5).of{ Item.make }
}}

Item.fix {{
    name:  /w{1}/.gen,
    count: rand(1..10),
    price: rand(100..1000)
}}
