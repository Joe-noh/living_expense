# -*- coding: utf-8 -*-

require_relative './spec_helper'

describe Expense do

  before do
    5.times { Expense.gen! }
  end

  describe 'DBスキーマ' do
    it '目的を持つ' do
      normals   = ["えんぴつ", "宴会", "party"]
      abnormals = ["", "  ", nil]

      validation_test(Expense, :purpose, normals, abnormals)
    end

    it '責任者を持つ' do
      normals   = ["トジマ", "jack"]
      abnormals = ["", "  ", nil]

      validation_test(Expense, :responsible, normals, abnormals)
    end

    it '1個以上のitemを持つ' do
      expense = Expense.first
      expense.items = []
      expect(expense).not_to be_valid
    end
  end

  describe 'メソッド' do
    it '総計を計算できる' do
      expense = Expense.first
      sum = expense.items.inject(0){|sum, item| sum + item.cost }
      expect(sum).to eql expense.total_cost
    end

    it '日本語で支払日を返せる' do
      expense = Expense.first
      expense.payday = Date.new(2013, 10, 31)
      expect(expense.readable_payday).to eql "2013年 10月 31日"
    end
  end
end
