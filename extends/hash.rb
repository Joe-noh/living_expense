class Hash
  def slice_as_symbol(*keys)
    keys.each_with_object({}){ |key, new_hash|
      new_hash[key.to_sym] = self[key] if self.has_key?(key)
    }
  end

  def except(*keys)
    self.reject{ |key, val| keys.member? key }
  end

  def symbolize_keys  # recursive
    self.inject({}){ |new_hash, (key, val)|
      new_hash[key.to_sym] = (val.is_a?(Hash) ? val.symbolize_keys : val)
      new_hash
    }
  end
end
