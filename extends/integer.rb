class Integer

  def comma_inserted
    self.to_s.reverse.scan(/.{1,3}/).join(',').reverse
  end

end
